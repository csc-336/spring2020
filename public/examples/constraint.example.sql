DROP DATABASE IF EXISTS Example;
CREATE DATABASE Example;
USE Example;

DROP TABLE IF EXISTS People;
CREATE TABLE People (
    Id INTEGER PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    Birthdate DATE,
    Mother INTEGER,
    Father INTEGER,
    FOREIGN KEY (Father) REFERENCES People(Id),
    FOREIGN KEY (Mother) REFERENCES People(Id)
);

-- Will not work in MYSQL: Check constraints cannot have sub-queries.
-- ALTER TABLE People ADD
--     CONSTRAINT `FatherBirthdayCheck` CHECK(Birthdate > (SELECT Birthdate FROM People WHERE Id=Father));

INSERT INTO People VALUES (1, 'Alice', '1800-01-01', NULL, NULL);
INSERT INTO People VALUES (2, 'Bob', '1800-02-02', NULL, NULL);
INSERT INTO People VALUES (3, 'Cathy', '1820-04-04', 1, 2);
INSERT INTO People VALUES (4, 'Diego', '1823-05-05', 1, 2);
INSERT INTO People VALUES (5, 'Abraham', '1810-10-10', NULL, NULL);
INSERT INTO People VALUES (6, 'Sarah', '1810-12-12', NULL, NULL);
INSERT INTO People VALUES (7, 'Isaac', '1830-02-02', 5, 6);
INSERT INTO People VALUES (8, 'Sam', '1850-02-02', 3, 7);
INSERT INTO People VALUES (9, 'Heather', '1855-02-02', 3, 7);
INSERT INTO People VALUES (10, 'Dana', '1812-02-02', NULL, NULL);
INSERT INTO People VALUES (11, 'Hana', '1850-05-05', 3, 10);

SELECT P1.Id,
       P1.Name,
       P1.Birthdate,
       P2.Name as MotherName,
       P2.Birthdate as MotherBDay,
       P3.Name as FatherName,
       P3.Birthdate as FatherBDay
    FROM People P1
    JOIN People P2 ON P2.Id = P1.Mother
    JOIN People P3 ON P3.Id = P1.Father;


SELECT P1.Name,
       P3.Name AS MotherName1,
       P4.Name AS FatherName1,
       P2.Name,
       P5.Name AS MotherName2,
       P6.Name AS FatherName2
    FROM People P1
    JOIN People P2 ON (P2.Father <> P1.Father AND P2.Mother = P1.Mother)
    JOIN People P3 ON P3.Id = P1.Mother
    JOIN People P4 ON P4.Id = P1.Father
    JOIN People P5 ON P5.Id = P2.Mother
    JOIN People P6 ON P6.Id = P2.Father;

--- A Different Example

CREATE TABLE Currency (
    Acronym CHAR(3) PRIMARY KEY,
    CONSTRAINT `AcronymFormatCheck` CHECK(Acronym REGEXP '[A-Z]{3}')
);

CREATE TABLE Store (
    Id INTEGER PRIMARY KEY,
    StreetAddress1 VARCHAR (255) NOT NULL,
    StreetAddress2 VARCHAR (255) NOT NULL,
    State CHAR (2) NOT NULL,
    Zip CHAR(10) NOT NULL,
    Currency CHAR(3) NOT NULL,
    CONSTRAINT `StateFormatCheck` CHECK(State REGEXP '[A-Z]{2}'),
    CONSTRAINT `ZipCodeFormatCheck` CHECK(Zip REGEXP '[0-9]{5}-[0-9]{4}'),
    FOREIGN KEY (Currency) REFERENCES Currency(Acronym)
);

CREATE TABLE Invertory (
    SKU INTEGER,
    Store INTEGER,
    Amount INTEGER NOT NULL,
    Price DECIMAL(13, 4) NOT NULL,
    Name VARCHAR(200) NOT NULL,
    PRIMARY KEY (SKU, Store),
    FOREIGN KEY (Store) REFERENCES Store(Id),
    CONSTRAINT `NonNegativePrice` CHECK(Price >= 0),
    CONSTRAINT `NonNegativeAmount` CHECK(Amount > 0)
);

CREATE TABLE Sale (
    Id INTEGER PRIMARY KEY,
    Store INTEGER,
    Tax DECIMAL(2, 2) NOT NULL,
    Currency CHAR(3) NOT NULL,
    At DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (Store) REFERENCES Store(Id),
    FOREIGN KEY (Currency) REFERENCES Currency(Acronym)
);

CREATE TABLE LineItem (
    Id INTEGER PRIMARY KEY AUTO_INCREMENT,
    SaleId INTEGER NOT NULL,
    SKU INTEGER NOT NULL,
    Store INTEGER NOT NULL,
    PricePerUnit DECIMAL(14, 4) NOT NULL,
    Amount INTEGER NOT NULL,
    UNIQUE KEY(SKU, SaleId),
    FOREIGN KEY (SaleId) REFERENCES Sale(Id),
    FOREIGN KEY (SKU, Store) REFERENCES Invertory (SKU, Store)
);

INSERT INTO Currency VALUES ('AUD');
INSERT INTO Currency VALUES ('GBP');
INSERT INTO Currency VALUES ('EUR');
INSERT INTO Currency VALUES ('JPY');
INSERT INTO Currency VALUES ('USD');

INSERT INTO Store VALUES (1, '1234 Foo St.', '', 'NY', '12345-6789', 'USD');
INSERT INTO Store VALUES (2, '5678 Bar Ave.', '', 'NJ', '44444-1111', 'USD');
-- These will fail due to violating the check contraints.
-- INSERT INTO Store VALUES (3, '1234 Bar St.', '', 'N1', '12345-6789');
-- INSERT INTO Store VALUES (4, '1234 Bar St.', '', 'NY', '123456789');

INSERT INTO Invertory VALUES (1000, 1, 10, 100.00, 'Animal Crossing NH');
INSERT INTO Invertory VALUES (2000, 1, 7, 10.00, 'Mine Craft');
INSERT INTO Invertory VALUES (3000, 1, 7, 10.00, 'Fortnight');
INSERT INTO Invertory VALUES (9999, 2, 6, 50.00, 'Animal Crossing NH');
INSERT INTO Invertory VALUES (5000, 2, 7, 10.00, 'Mine Craft');

INSERT INTO Sale VALUES (1, 1, 0.1, 'USD', NOW());
INSERT INTO Sale VALUES (2, 1, 0.1, 'USD', NOW());
INSERT INTO LineItem VALUES (NULL, 1, 1000, 1, 100.00, 1);
INSERT INTO LineItem VALUES (NULL, 1, 2000, 1, 10.00, 1);
INSERT INTO LineItem VALUES (NULL, 1, 3000, 1, 10.00, 1);

SELECT Sale.Id,
       Sale.Store,
       Sale.Tax,
       Sale.Currency,
       Sale.At,
       SUM(Item.PricePerUnit * Item.Amount) * (1 + Sale.Tax)
        FROM Sale JOIN LineItem Item ON Item.SaleId = Sale.Id;


